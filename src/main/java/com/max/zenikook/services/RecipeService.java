package com.max.zenikook.services;

import com.max.zenikook.controllers.representation.NewRecipeRepresentation;
import com.max.zenikook.domains.Recipe;
import com.max.zenikook.repository.recipeRepository;
import com.max.zenikook.services.factory.NewRecipe;
import com.max.zenikook.services.factory.RecipeFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RecipeService {
    private recipeRepository recipeRepository;
    private idGenerator idGenerator;
    private RecipeFactory factory;

    public RecipeService(com.max.zenikook.repository.recipeRepository recipeRepository, com.max.zenikook.services.idGenerator idGenerator, RecipeFactory factory) {
        this.recipeRepository = recipeRepository;
        this.factory = factory;
        this.idGenerator = idGenerator;
    }

    public List<Recipe> getAllRecipes() {
        return (List<Recipe>) this.recipeRepository.findAll();
    }

    public Optional<Recipe> getOneRecipe(String id) {
        return this.recipeRepository.findById(id);
    }

    public void deleteRecipe(String id) {
        this.recipeRepository.deleteById(id);
    }

    public Recipe createRecipe(NewRecipe nrc) {
        return this.recipeRepository.save(factory.createRecipe(nrc));
    }

}
