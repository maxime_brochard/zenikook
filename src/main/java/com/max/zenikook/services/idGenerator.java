package com.max.zenikook.services;

import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.UUID.randomUUID;

@Component
public class idGenerator { ;
    public String generateNewId() {
        String id = UUID.randomUUID().toString();
        return id;
    }
}

