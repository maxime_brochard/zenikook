package com.max.zenikook.services.factory;

import com.max.zenikook.controllers.representation.NewRecipeRepresentation;
import com.max.zenikook.domains.Recipe;
import com.max.zenikook.services.idGenerator;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RecipeFactory {
    private idGenerator id;

    public RecipeFactory(idGenerator idGenerator) {
        this.id = idGenerator;
    }

    public Recipe createRecipe(NewRecipe newRecipe) {
        Recipe toCreate = new Recipe(id.generateNewId(), newRecipe.getName(), newRecipe.getRecipe_content());
        return toCreate;
    }
}
