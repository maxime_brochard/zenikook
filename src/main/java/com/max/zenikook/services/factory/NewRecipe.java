package com.max.zenikook.services.factory;

public class NewRecipe {
    String id;
    String name;
    String recipe_content;

    public NewRecipe(String id, String name, String recipe_content) {
        this.id = id;
        this.name = name;
        this.recipe_content = recipe_content;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipe_content(String recipe_content) {
        this.recipe_content = recipe_content;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRecipe_content() {
        return recipe_content;
    }


}
