package com.max.zenikook.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "recipes")
public class Recipe {
    @Id
    private String id;
    private String name;
    private String recipeContent;

    protected Recipe(){}

    public Recipe(String id, String name, String recipeContent) {
        this.id = id;
        this.name = name;
        this.recipeContent = recipeContent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }
}
