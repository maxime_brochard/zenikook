package com.max.zenikook.repository;

import com.max.zenikook.domains.Recipe;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface recipeRepository extends CrudRepository<Recipe, String> {

}
