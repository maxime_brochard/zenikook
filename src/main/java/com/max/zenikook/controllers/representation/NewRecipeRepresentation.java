package com.max.zenikook.controllers.representation;

public class NewRecipeRepresentation {
    String name;
    String recipe_content;

    public void setName(String name) {
        this.name = name;
    }

    public void setRecipe_content(String recipe_content) {
        this.recipe_content = recipe_content;
    }

    public String getName() {
        return name;
    }

    public String getRecipe_content() {
        return recipe_content;
    }
}
