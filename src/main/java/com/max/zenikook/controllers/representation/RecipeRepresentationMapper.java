package com.max.zenikook.controllers.representation;

import com.max.zenikook.domains.Recipe;
import org.springframework.stereotype.Component;

@Component
public class RecipeRepresentationMapper {

    public DisplayableRecipeRepresentation mapToDisplayableRecipe(Recipe recipe) {

        DisplayableRecipeRepresentation result = new DisplayableRecipeRepresentation();
        result.setId(recipe.getId());
        result.setName(recipe.getName());
        result.setRecipeContent(recipe.getRecipeContent());

        return result;
    }
}
