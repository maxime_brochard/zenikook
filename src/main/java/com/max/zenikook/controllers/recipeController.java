package com.max.zenikook.controllers;

import com.max.zenikook.controllers.representation.DisplayableRecipeRepresentation;
import com.max.zenikook.controllers.representation.NewRecipeRepresentation;
import com.max.zenikook.controllers.representation.RecipeRepresentationMapper;
import com.max.zenikook.domains.Recipe;
import com.max.zenikook.services.RecipeService;
import com.max.zenikook.services.factory.NewRecipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/recipes")
public class recipeController {

    private RecipeService recipeService;
    private RecipeRepresentationMapper mapper;

    @Autowired
    public recipeController(RecipeService recipeService, RecipeRepresentationMapper recipeRepresentationMapper) {
        this.recipeService = recipeService;
        this.mapper = recipeRepresentationMapper;
    }

    @GetMapping
    List<DisplayableRecipeRepresentation> getAllRecipes() {
        return this.recipeService.getAllRecipes().stream()
                .map(this.mapper::mapToDisplayableRecipe)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<DisplayableRecipeRepresentation> getRecipeById(@PathVariable("id") String id) {
        Optional<Recipe> recipe = this.recipeService.getOneRecipe(id);

        return recipe.
                map(this.mapper::mapToDisplayableRecipe)
                .map(ResponseEntity::ok)// Optional<ResponseEntity<Question>>
                .orElseGet(() -> ResponseEntity.notFound().build());// ResponseEntity<Question>
    }

    @PostMapping("/{id}/:deleteRecipe")
    public ResponseEntity<DisplayableRecipeRepresentation> deleteRecipe(@PathVariable("id") String id) {
        this.recipeService.deleteRecipe(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public DisplayableRecipeRepresentation createRecipe(@RequestBody NewRecipe body) {
        final Recipe toBeCreatedRecipe;
        if (body != null) {
            toBeCreatedRecipe = this.recipeService.createRecipe(body);
        } else {
            throw new IllegalArgumentException("Please provide a body");
        }
        ;
        return this.mapper.mapToDisplayableRecipe(toBeCreatedRecipe);
    }
}

