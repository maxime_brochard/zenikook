drop table if exist recipes;
create table recipes
(
id text primary key,
name text not null,
recipe_content text not null
);